import tempfile
from pprint import pprint

import PIL
import cv2
import fitz
import layoutparser
import numpy
import numpy as np
import shapely
from PIL.Image import Image
from pytesseract import pytesseract


def text_block_to_layout_block(block):
    return layoutparser.elements.TextBlock(
        layoutparser.elements.Rectangle(
            *block[0:4]
        ),
        text=block[4]
    )


def layout_block_to_shapely(block: layoutparser.elements.TextBlock) -> shapely.Polygon:
    return shapely.Polygon((
        (block.block.x_1, block.block.y_1),
        (block.block.x_1, block.block.y_2),
        (block.block.x_2, block.block.y_2),
        (block.block.x_2, block.block.y_1),
    ))



# model = layoutparser.models.Detectron2LayoutModel(
#     config_path='lp://efficientdet/PubLayNet/config',  # In model catalog
#     label_map={0: "Text", 1: "Title", 2: "List", 3: "Table", 4: "Figure"},  # In model`label_map`
#     extra_config=["MODEL.ROI_HEADS.SCORE_THRESH_TEST", 0.5]  # Optional
# )
model = layoutparser.models.AutoLayoutModel("lp://efficientdet/PubLayNet/tf_efficientdet_d1")

class_weight = {
    "Text": 1.0,
    "Figure": 1.0,
    "List": 1.0,
    "Title": 3.0
}


def pil_to_cv2(pimg: Image):
    numpy_image = numpy.array(pimg)
    opencv_image = cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)
    return opencv_image


def cv2_to_pil(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = PIL.Image.fromarray(img)
    return im_pil


def tesseract_prepare(image):
    image = cv2.resize(image, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_CUBIC)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    kernel = np.ones((1, 1), np.uint8)
    image = cv2.dilate(image, kernel, iterations=1)
    image = cv2.erode(image, kernel, iterations=1)
    return image  # cv2.threshold(cv2.bilateralFilter(image, 5, 75, 75), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[-1]


def ocr_layout_block(image: Image, block: layoutparser.elements.TextBlock):
    return pytesseract.image_to_data(
        cv2_to_pil(
            tesseract_prepare(
                pil_to_cv2(
                    image.crop((
                        block.block.x_1,
                        block.block.y_1,
                        block.block.x_2,
                        block.block.y_2
                    ))
                )
            )
        ), output_type=pytesseract.Output.DICT)


def extract_text_and_classify(page):
    pix = page.get_pixmap()
    image = PIL.Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
    layout = model.detect(image)

    tp = page.get_textpage()

    lbs = []
    lbs_tb_intersections = {
        i: 0.0
        for i, layout_block in enumerate(layout._blocks)
    }
    for text_block in tp.extractBLOCKS():
        lb = text_block_to_layout_block(text_block)
        geom = layout_block_to_shapely(lb)
        block_classes = {
            "Text": 0.5
        }

        for i, layout_block in enumerate(layout._blocks):
            lb_geom = layout_block_to_shapely(layout_block)
            if shapely.intersects(geom, lb_geom):
                intersection = shapely.area(shapely.intersection(geom, lb_geom)) / shapely.area(geom)
                iintersection = shapely.area(shapely.intersection(geom, lb_geom)) / shapely.area(lb_geom)
                block_classes[layout_block.type] = intersection * class_weight.get(layout_block.type, 1.0)
                lbs_tb_intersections[i] = lbs_tb_intersections.get(i, 0.0) + iintersection

        print(block_classes)
        block_class = max(block_classes, key=block_classes.get)
        lb.type = block_class
        lbs.append(lb)

    pprint(lbs_tb_intersections)

    for i, lb in enumerate(layout._blocks):
        if lbs_tb_intersections[i] < 0.1:
            lb.text = " ".join(ocr_layout_block(image, layout._blocks[i])['text'])
            lbs.append(lb)

    return lbs


def unstructured_parsing(file_path: str) -> layoutparser.elements.Layout:
    lbs_all = []
    with fitz.open(file_path) as doc:
        for i, page in enumerate(doc.pages()):
            print('Processing ' + str(i + 1) + ' of ' + str(doc.page_count))
            lbs = extract_text_and_classify(page)

            pix = page.get_pixmap()
            image = PIL.Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
            lbs_all += lbs
    layout = layoutparser.elements.Layout(lbs_all)
    return layout
