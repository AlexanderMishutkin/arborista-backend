## Build

```bash
docker build -t arborista-back .
```

## Run

```bash
docker run -p 5000:5000 arborista-back
```
