import base64
import tempfile
from io import BytesIO
from pprint import pprint
from typing import List, Generator

import PIL
import cv2
import fitz
import layoutparser
import layoutparser as lp
import numpy
import numpy as np
import shapely
from PIL.Image import Image
from pytesseract import pytesseract
import re


class LayoutContainer(layoutparser.elements.TextBlock):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._features = self._features + [
            "coordinates",  # (self.x_1, self.y_1, self.x_2, self.y_2)
        ]


def text_block_to_layout_block(block):
    return layoutparser.elements.TextBlock(
        layoutparser.elements.Rectangle(
            *block[0:4]
        ),
        text=block[4]
    )


def layout_block_to_shapely(block: layoutparser.elements.TextBlock) -> shapely.Polygon:
    return shapely.Polygon((
        (block.block.x_1, block.block.y_1),
        (block.block.x_1, block.block.y_2),
        (block.block.x_2, block.block.y_2),
        (block.block.x_2, block.block.y_1),
    ))


model = layoutparser.models.AutoLayoutModel("lp://efficientdet/PubLayNet/tf_efficientdet_d1")
        # lp.models.Detectron2LayoutModel(
        #     config_path ='lp://PubLayNet/faster_rcnn_R_50_FPN_3x/config', # In model catalog
        #     label_map   ={0: "Text", 1: "Title", 2: "List", 3:"Table", 4:"Figure"}, # In model`label_map`
        #     extra_config=["MODEL.ROI_HEADS.SCORE_THRESH_TEST", 0.3] # Optional
        # ) # layoutparser.models.AutoLayoutModel("lp://efficientdet/PubLayNet/tf_efficientdet_d1")

class_weight = {
    "Text": 1.0,
    "Figure": 1.0,
    "List": 1.0,
    "Title": 3.0,
    "Figure": 0.001
}


def pil_to_cv2(pimg: Image):
    numpy_image = numpy.array(pimg)
    opencv_image = cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)
    return opencv_image


def cv2_to_pil(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = PIL.Image.fromarray(img)
    return im_pil


def tesseract_prepare(image):
    image = cv2.resize(image, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_CUBIC)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    kernel = np.ones((1, 1), np.uint8)
    image = cv2.dilate(image, kernel, iterations=1)
    image = cv2.erode(image, kernel, iterations=1)
    return image  # cv2.threshold(cv2.bilateralFilter(image, 5, 75, 75), 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[-1]


def ocr_layout_block(image: Image, block: layoutparser.elements.TextBlock):
    return pytesseract.image_to_data(
        cv2_to_pil(
            tesseract_prepare(
                pil_to_cv2(
                    image.crop((
                        block.block.x_1,
                        block.block.y_1,
                        block.block.x_2,
                        block.block.y_2
                    ))
                )
            )
        ), output_type=pytesseract.Output.DICT)


headers_stack = []
must_chains = [
    ['Contents'],
    #  ['Contents', 'Executive Summary'],
    #  ['Contents', 'Background and rationale'],
    #  ['Contents', 'Accompanying documents'],
    ['Executive Summary'],
    ['Background and rationale'],
    ['Accompanying documents'],
    ['Accompanying documents', 'Draft cost-benefit analysis'],
    ['Accompanying documents', 'Summary of responses to the consultation'],

]


def count_dots(line):
    match = re.match(r'^\d+(\.\d+)*', line)
    if match:
        return match.group(0).count('.')
    return 0


def find_parent(lb):
    global headers_stack
    if not headers_stack and lb.type == 'Title':
        headers_stack.append(lb)
        return None
    for chain in must_chains:
        if chain[0] in lb.text:
             headers_stack = [lb]
             lb.type = 'Title'
             return None
        if len(chain) > 1 and headers_stack and chain[0] in headers_stack[0].text and chain[1] in lb.text:
             headers_stack.append(lb)
             lb.type = 'Title'
             return headers_stack[0]
    if lb.type != 'List':
        headers_stack = [
            header for header in headers_stack
            if header.type != 'List'
        ]
    else:
        while headers_stack:
            if headers_stack[-1].type == 'List' and count_dots(headers_stack[-1].text) >= count_dots(lb.text):
                headers_stack = headers_stack[:-1]
            else:
                break
        headers_stack.append(lb)
        return headers_stack[-2] if len(headers_stack) > 1 else None

    if lb.type == 'Title':
        while headers_stack:
            if headers_stack[-1].block.x_1 <= lb.block.x_1:
              headers_stack = headers_stack[:-1]
            else:
              break
        headers_stack.append(lb)
        return headers_stack[-2] if len(headers_stack) > 1 else None
    return headers_stack[-1] if headers_stack else None


def extract_text_and_classify(page):
    pix = page.get_pixmap()
    image = PIL.Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
    layout = model.detect(image)

    tp = page.get_textpage()

    lbs = []
    lbs_tb_intersections = {
        i: 0.0
        for i, layout_block in enumerate(layout._blocks)
    }

    for text_block in tp.extractBLOCKS():
        lb = text_block_to_layout_block(text_block)
        geom = layout_block_to_shapely(lb)
        block_classes = {
            "Text": 0.5
        }

        for i, layout_block in enumerate(layout._blocks):
            lb_geom = layout_block_to_shapely(layout_block)
            if shapely.intersects(geom, lb_geom):
                intersection = shapely.area(shapely.intersection(geom, lb_geom)) / shapely.area(geom)
                iintersection = shapely.area(shapely.intersection(geom, lb_geom)) / shapely.area(lb_geom)
                block_classes[layout_block.type] = intersection * class_weight.get(layout_block.type, 1.0)
                lbs_tb_intersections[i] = lbs_tb_intersections.get(i, 0.0) + iintersection

        # print(block_classes)
        block_class = max(block_classes, key=block_classes.get)
        lb.type = block_class

        lb.parent = find_parent(lb)

        lbs.append(lb)

    return lbs


class ArboristaLayout:
    def __init__(self, blocks: List[layoutparser.elements.TextBlock], image_str: str):
        self.blocks = blocks
        self.image_str = image_str


def structured_parsing(file_path: str) -> Generator[ArboristaLayout, None, None]:
    global headers_stack
    headers_stack = []
    with fitz.open(file_path) as doc:
        for i, page in enumerate(doc.pages()):
            print('Processing ' + str(i + 1) + ' of ' + str(doc.page_count))
            lbs = extract_text_and_classify(page)

            pix = page.get_pixmap()
            image = PIL.Image.frombytes("RGB", [pix.width, pix.height], pix.samples)
            image = lp.draw_box(image, layoutparser.elements.Layout(lbs), box_width=3)

            buffered = BytesIO()
            image.save(buffered, format="JPEG")
            img_bs64 = base64.b64encode(buffered.getvalue())
            yield ArboristaLayout(lbs, "data:image/jpeg;base64," + img_bs64.decode("utf-8"))
