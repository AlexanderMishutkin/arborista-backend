import base64
import os
import tempfile

import requests
from flask import Flask, jsonify, request

from algorithm import unstructured_parsing
from structured_parsing_naive import structured_parsing as structured_parsing_naive


app = Flask(__name__)


def request_gpt3(text):
    try:
        url = "http://ec2-54-93-222-69.eu-central-1.compute.amazonaws.com:5000/generate_text"

        payload = text
        headers = {
            "Content-Type": "text/plain",
            "User-Agent": "insomnia/2023.5.8"
        }

        response = requests.request("POST", url, data=payload, headers=headers)
        return response.text
    except Exception as e:
        return str(e)


def summarize(layout):
    for i, block in enumerate(layout):
        if block["type"] == 'Title':
            blocks = ""
            for j in range(i, len(layout)):
                if layout[j]["type"] != 'Title' or i == j:
                    blocks += layout[j]["text"] + "\n\n"
                else:
                    break
            summary = request_gpt3(blocks)
            block['summary'] = summary
    return layout


def parse_pdf_from_data_url(data_url):
    # Extract base64 encoded data from the data URL
    pdf_data = base64.b64decode(data_url.split(',')[1])

    # Save pdf temporary file
    with tempfile.NamedTemporaryFile(delete=False) as temp_file:
        temp_file.write(pdf_data)
        path = temp_file.name

    layout = unstructured_parsing(path)
    os.remove(path)

    res = {
        "layout": summarize([
            {
                "text": block.text,
                "type": block.type
            }
            for block in layout._blocks
        ])
    }
    return res


def parse_pdf_from_data_url_all_info(data_url):
    # Extract base64 encoded data from the data URL
    pdf_data = base64.b64decode(data_url.split(',')[1])

    # Save pdf temporary file
    with tempfile.NamedTemporaryFile(delete=False) as temp_file:
        temp_file.write(pdf_data)
        path = temp_file.name

    res = {
        "layout": [
        ]
    }
    for layout in structured_parsing_naive(path):
        res["layout"].append({
            "image": layout.image_str,
            "blocks": [
                {
                    "text": block.text,
                    "type": block.type,
                    "coordinates": block.coordinates,
                }
                for block in layout.blocks
            ]
        })
    os.remove(path)
    return res


@app.route('/parse-pdf', methods=['POST'])
def parse_pdf_endpoint():
    data_url = request.json.get('data_url', '')
    parsed_text = parse_pdf_from_data_url(data_url)
    return jsonify({'result': parsed_text})


@app.route('/full-info', methods=['POST'])
def full_info_endpoint():
    data_url = request.json.get('data_url', '')
    parsed_text = parse_pdf_from_data_url_all_info(data_url)
    return jsonify({'result': parsed_text})


app.run(port=5000, debug=False, host='0.0.0.0')
