FROM python:3.10-slim-bookworm

ENV PYHTONUNBUFFERED=1

RUN apt-get update
RUN apt-get -y install tesseract-ocr

WORKDIR /app

COPY ./requirements.txt /app
RUN apt-get -y install git
RUN apt-get -y install build-essential
RUN --mount=type=cache,target=/root/.cache/pip pip3 install -r requirements.txt
RUN pip install 'git+https://github.com/facebookresearch/detectron2.git@v0.4#egg=detectron2'
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y

RUN pip install Werkzeug==3.0.2

COPY . /app

ENTRYPOINT ["python3"]
CMD ["app.py"]
